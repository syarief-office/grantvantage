/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	jQuery('body').fadeIn(2000);
	jQuery('#dvLoading').fadeOut(4000);	

	jQuery(window).bind("load scroll", function(){
		var sticky_offset = jQuery('.headermain').height() + 20;
		var scroll_top = jQuery(window).scrollTop();
		var wSize = jQuery(window).width();

		if(wSize > 797){
			if (scroll_top > sticky_offset) { 
				jQuery('#stickyhead').css("top", 0);
			}else{
				jQuery('#stickyhead').css("top", "-500px");
			} 	
		}
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();

    //BXLSIDER 
    jQuery('.bxslider').bxSlider({
	  adaptiveHeight: true,
	  auto: true,
	  pager: false
	});

	//RESPONSIVE NAV
	jQuery('#menu').slicknav();

	//ANIMATE
	new WOW().init();

	jQuery("img").lazyload();

	revapi = jQuery('.tp-banner').revolution(
	{
		delay:9000,
		startwidth:1170,
		startheight:600,
		hideThumbs:10,
		fullWidth:"on",
		forceFullWidth:"on",
		onHoverStop:"off",
		navigationType:"off",
	});

	//CHECKBOX
	jQuery('input[type=checkbox].cxbox, input[type=radio].radio').customRadioCheck();

	jQuery('.dropdown-toggle').dropdown();

	jQuery(document).on('open', '#remodal', function () {
        console.log('open');
    });

    jQuery('.selectpicker').selectpicker();


});
/* END ------------------------------------------------------- */